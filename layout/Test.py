from Sensor import Sensor

class One :

    def __init__(self,name) :
        self.name = name;
    
    def message(self, value) :
        print self.name, value


class Two:

    def __init__(self,name) :
        self.name = name;
    
    def whatever(self, value) :
        print self.name, value, value

        
o1 = One("Ich")
o2 = One("Du")
t1 = Two("Er")
    
s = Sensor(11)

s.register(o1.message)
s.register(o2.message)
s.register(t1.whatever)

s.update("Nix")
        
