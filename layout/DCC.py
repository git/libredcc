import Common as common

import logging as log
log.basicConfig(level=log.DEBUG)

class Packet(object) :

    def __init__(self, packet) :
        self.packet = packet

    def __repr__(self) :
        s = map(lambda b : "{:02x}".format(b), self.packet)
        return " ".join(s)
                
    def update_checksum(self) :
        """TODO more pythonish with reduce?
        """

        xor = 0
        for byte in self.packet[:-1] :
            xor^= byte
        self.packet[-1] = xor

IDLE = Packet([0xFF, 0x00, 0xFF])
RESET = Packet([0x00, 0x00, 0x00])

class BA_Packet(Packet) :
    def __init__(self, address, state = 0) :
        """
        @param address 11bits of DCC address
        """
        port = address & 0x3 
        addr_l = (address >> 2) & 0x3F  # 6 bits
        addr_h_not = (~(address >> 8)) & 0x7 # 3 more bits

        Packet.__init__(self, [ (0x80 | addr_l ), (0x88 | port << 1 | (addr_h_not << 4)) , 0])
        self.set_gate(state)

    def get_gate(self) :
        return self.packet[1] & 0x1
    def set_gate(self, g) :
        self.packet[1] &= ~ (0x1)
        self.packet[1] |= 1 if g else 0
        self.upate_checksum()
    state = property(get_gate, set_gate)
    
        

    

class MF14_Packet(Packet) :

    def __init__(self,
                 address,
                 direction = common.Direction.FORWARDS,
                 speed=0) :
        """
        """
        log.debug("constr")
        Packet.__init__(self, [address & 0x7F, 0x3F, 0,0])

        if address > 0x7F :
            log.warning("Short DCC addresses exceed range: {}".format(address))
        #self.set_speed(speed)
        #self.set_dir(direction)
        self.speed = speed
        self.dir = direction

    def get_speed(self):
        log.debug("Speed getter")
        v = self.packet[2] & 0x7F
        if v > 0 : v-= 1
        return v

    def set_speed(self, v):
        log.debug("Speed setter")
        if v > 0x7E :
            log.warning("Max speed step exceeded <{}>".format(v))
            v = 0x7F
        elif v > 0 : v += 1
        self.packet[2] &= ~(0x7F)
        self.packet[2] |= v
        self.update_checksum()
    speed = property(get_speed, set_speed)

    def get_dir(self):
        log.debug("Dir Getter")
        d = self.packet[2] & 0x80
        return common.Direction.FORWARDS if d else common.Direction.BACKWARDS

    def set_dir(self, d):
        log.debug("Dir Setter")
        self.packet[2] &= 0x7f
        self.packet[2] |= 0x80 if d == common.Direction.FORWARDS else 0
        self.update_checksum()
    dir = property(get_dir, set_dir)


class FuncOne_Packet(Packet) :
    def __init__(self, address) :
        Packet.__init__(self, [address & 0x7F, 0x80, 9])
        self.update_checksum()


if __name__== "__main__" :
    print BA_Packet(1, 1)
    print BA_Packet(1, 0)
    print FuncOne_Packet(1)
    m = MF14_Packet(1,0)
    print m
    print m.speed
    m.speed = 16
    print m.speed
    
    
