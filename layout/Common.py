from enum import Enum

class Direction(Enum) :
        FORWARDS = 0
        BACKWARDS = 1

