import networkx as nx
import matplotlib.pyplot as plt

from SensorManager import SensorManager, Hsi88SensorManager
from AccessoryManager import AccessoryManager
from Block import Block
from Route import Route
from Resources import Points

import logging

#logging.basicConfig(filename='example.log', filemode='w')
logging.basicConfig(level=logging.DEBUG)

sm = SensorManager()
#sm = Hsi88SensorManager("/dev/ttyACM1")
am = AccessoryManager("/dev/ttyACM0")


left = Block(
    name="Left",
    downSensor = sm.provideSensor(1),
    upSensor = sm.provideSensor(2),
    upSignal = am.provideSignal("A", None),
)
main = Block(
    name="Main",
    downSensor = sm.provideSensor(3),
    upSensor = sm.provideSensor(4),
    downSignal = am.provideSignal("P1", None),
    upSignal = am.provideSignal("N1", None),
)

loop = Block(
    name="Loop",
    downSensor = sm.provideSensor(5),
    upSensor = sm.provideSensor(6),
    downSignal = am.provideSignal("P2", None),
    upSignal = am.provideSignal("N2", None)
)

right = Block(
    name="Right",
    downSensor = sm.provideSensor(7),
    upSensor = sm.provideSensor(8),
    downSignal = am.provideSignal("F", None)
)

w1 = am.providePoints("W1", None)
w2 = am.providePoints("W2", None)

left2main = Route(
    name = "left2main",
    down = left,
    up = main,
    interlocking = { w1 : Points.State.STRAIGHT }
)

left2loop = Route(
    name = "left2loop",
    down = left,
    up = loop,
    interlocking = { w1 : Points.State.THROWN }
)

main2right = Route(
    name = "main2right",
    down = main,
    up = right,
    interlocking = {w2 : Points.State.STRAIGHT }
)

loop2right = Route(
    name = "loop2right",
    down = loop,
    up = right,
    interlocking = {w2, Points.State.THROWN }
)
    
    
L = nx.MultiDiGraph()
L.add_nodes_from( [left, main, loop, right])

L.add_edges_from([
#    (left, main, route :left2main),
#    (left, loop) route = left2loop, 
#    (main, right), route = main2right, 
#    (loop, right), route = loop2main,
])

left2main.set("Zug1")
#left2loop.set("Zug2")
main2right.set("Zug3")
left2main.release("Zug1")
left2main.set("Zug4")


nx.draw_networkx(L)
plt.show()
