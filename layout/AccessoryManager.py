from Resources import Points, Signal

class AccessoryManager :
    """
    TODO avoid that provideSignal may return an Points and vice-versa
    TODO do we need this intermediate class?
    """
    def __init__(self, cs) :
        self.accessories = dict()
        self.cs = cs

    def providePoints(self, name, address) :
        if address not in self.accessories :
            self.accessories[address] = Points(name, self.cs, address)
        return self.accessories[address]


    def provideSignal(self, name, address) :
        if address not in self.accessories :
            self.accessories[address] = Signal(name, self.cs, address)
        return self.accessories[address]

        

        

