from enum import Enum

class Loco :

    def get_speed(self):
        return self.lm.get_speed(self)
    def set_speed(self, speed):
        self.lm.set_speed(self,speed)
    speed = property(get_speed, set_speed, "Speed of the loco.")

    def get_dir(self):
        return self.lm.get_dir(self)
    #def set_dir(self, dir):
    #    self.lm.set_dir(self,dir)
    #dir = property(get_dir, set_dir, "Direction of the loco.")

    dir = property(lambda : self.lm.get_dir(self),
                   lambda d : self.lm.set_dir(self,d),
                   "Direction of the loco.")



    def __init__(self, name, lm, address,
    ) :

        # convenience
        self.name = name

        # static
        self.address = address
        self.lm = lm # holds the dynamic data

class LocoManager :

    def __init__(self) :
        self.locos = {}

    def provideLoco(name, address) :

        key = (name, address)
        if key not in self.locos :
            locos[key] = new Loco(name, address)
        return locos[key]

class DCCLocoManager :

    def __init__(self, cs) :
        LocoManager.__init__(self)
        self.packets = {}
    
    def provideLoco(name, address) :
        loco = LocoManager.provideLoco(self,name,address)
        if loco not in self.packets :
            packets[Loco] = (
                MF14_Packet(address),
                FuncOne_Packet(address)
                )
        return loco

    def get_dir(self, loco) :
        return locos[loco][0].dir

    def set_dir(self, loco, d) :
        locos[loco][0].dir = d

    def get_speed(self, loco, v) :
        return locos[loco][0].speed

    def set_speed(self, loco, v) :
        locos[loco][0].speed = v

            
            
            
        
        

        



        

        

        
