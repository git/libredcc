from enum import Enum

import logging

class Resource :
    """Abstraction of a layout resource such as points, signals and
    sections of trakc that can be acquired and locked by multiple
    users, eg a bt a route.   
    """

    def __init__(self) :
        """create a lockable resources and initialise its state
        """
        self.owners = set()
        self.state = None

    def __str__(self) :
        return str(self.owners) + ", " + str(self.state)
        
    def lock(self, route, v) :
        """lock this resourse in state v
        @return false if unsucessful
        """

        # if as desired, just add acquirer to the list of owners:
        if self.state == v :
            self.owners.add(route)
            return True;

        # if other owners, nothing we can do:
        if len(self.owners) != 0 :
            logging.info("{} is already locked by {}".format(self, self.owners))
            return False;

        # no other owner: need to set on the layout:
        self.owners.add(route)
        logger.debug("Setting {} to {} on layout".format(self, v))
        return self.set(v) == v

    def unlock(self, owner) :
        """release resouce, ie unlock
        """
        self.owners.discard(owner)

class Points(Resource) :
    """Abstraction of a set of points (AKA switch, turnout).
    """

    class State(Enum) : 
        STRAIGHT = 0
        THROWN = 1
        UNKNOWN = 2

    def __init__(self, name, cs, address) :
        Resource.__init__(self)

        self.name = name
        self.address = address
        self.cs = cs
        self.state = Points.State.UNKNOWN

    def __str__(self) :
        return "Points " + str(self.name) + ": " + str(self.address) + ", " + str(self.state) + "| " + Resource.__str__(self)

    def set(self, v) :
        """to be overriden for the lowlevel stuff
        """
        self.state = v
        return v

class Signal :

    class Aspect(Enum) :
        UNKNOWN = -1
        STOP = 0
        PROCEED = 1

        # for DB signals:
        HP0 = STOP 
        HP1 = PROCEED

    def __init__(self, name, cs, address) :

        # convenience
        self.name = name


        
        # static data
        self.cs = cs
        self.address = address

        # dynamic data
        self.owner = None
        self.aspect = Signal.Aspect.UNKNOWN

    def __str__(self) :
        return "Signal " + self.name + ": " + str(self.owner) + ", " + str(self.aspect)

    def lock(self, route) :

        if self.owner == None or self.owner == route:
            self.owner = route
            return True
        else :
            logging.info("{} not held by {}.".format(self, route)
            return False

    def unlock(self, route) :
        if route == self.owner :
            self.owner = None
        else :
            logging.info("{} not held by {}.".format(route, self.owner))
            

    def set(self, aspect) :
        if self.aspect == aspect :
            return True
        else :
            logging.debug("Setting {} to {}".format(self, aspect)
            cs.ba(address, 1 if aspect == Signal.Aspect.PROCEED else 0)
            self.aspect = aspect

    def stop(self) :
        self.set(Signal.Aspect.STOP)

    
        
