from enum import Enum

import logging

class Sensor :
    """ Abstraction of a Layout Sensor -- ie any feedback device that
    returns information from the layout.
    """

    class State(Enum) :
        INACTIVE = 0
        ACTIVE = 1

    def __init__(self, address) :
        """Create a new sensor with given hardware address.
        """
        self.observers = set()
        self.oneTimeObservers = set()
        self.address = address
        # TODO register with sensor manager?

    def __str__(self) :
        return "Sensor " + str(self.address)
        
    def register(self, method) :
        """Register a (bound) method with one free parameter to
        receive updates of this sensor.
        """
        self.observers.add(method)

    def registerOnce(self, condition, method) :
        """Register a (bound) method with one free parameter to
        recieve updates of this sensor. A registered method is
        executed and unregistered if its condition(value) evaluates to True.
        """
        self.oneTimeObservers.add( (condition,method) )

    def update(self, value) :
        """1. Notify all registered observers of a new value of this
        Sensor. The registered methods are called as method(value).
        2. Notify all one time obsevers is their condition is met.
        The condition is called as condition(value). If this evaluates
        to true, their method is executed as method(), and
        subsequently removed from oneTimeObservers.
        """

        logging.debug("{} updated to {}".format(self, value))
        
        for o in self.observers:
            o(value)

        due = {o for o in self.oneTimeObservers if o[0](value) }
        for o in due:
            o[1]()
        self.oneTimeObservers -= due
        

        




                                   
            

        
