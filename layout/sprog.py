#!/usr/bin/python
# -*- coding: latin-1 -*-

#from AccessoryManager import AccessoryManager
#import Accessory 
from CommandStation import CommandStation
import DCC


import serial

import logging as log
log.basicConfig(level=log.DEBUG)

class Sprog(CommandStation) :

    def __init__(self, device_name) :
        CommandStation.__init__(self)
        #AccessoryManager.__init__(self)

        self.dev = serial.Serial(device_name,
                                115200,
                                timeout = 10
                                )

        # Read welcome message and remove any garbage
        # welcome = self.dev.read_until('\r')
        # logging.info("Sprog Welcome: <{}>".format(welcome))
        self.dev.write("\r")
        self.await_prompt()
        if self.dev.in_waiting:
            garbage = self.dev.readall()
            log.warn("Collecting garbage input: <{}>".format(garbage))

    def await_prompt(self) :
        """TODO check for error messages!
        """
        # prompt ends in '> \r'
        prompt = self.dev.read_until('> ')
        log.debug("Prompt: <{}>".format(prompt))

        # did we time out? or get the correct prompt?
        if prompt[-2:] != '> ' :
            log.warn("Unexpeced prompt: <{}>".format(prompt.strip()))
            if self.dev.in_waiting:
                garbage = self.dev.readall()
                log.warn("Collecting garbage input: <{}>".format(garbage.strip()))
            return False
        

        # prompt should have either R or P at third last position:
        if (prompt[-3] != 'P') and (prompt[-3] != 'R') :
            log.warn("Unexpected status letter in prompt: <{}>".format(prompt[-3]))
            if self.dev.in_waiting :
                garbage = self.dev.readall()
                log.warn("Collecting garbage input: <{}>".format(garbage.strip()))
            return False
        
        return True


    def send(self, packet) :
        """
        """
        CommandStation.send(self,packet)

        s = "O " + str(packet) + "\r"
        log.debug("Packet to send: {}", s)
        self.dev.write(s)

        return self.await_prompt()

    def power(self, value) :

        CommandStation.power(self,value)

        # switch power
        cmd = "+\r" if value else "-\r"
        self.dev.write(cmd)

        return self.await_prompt()

    def ba(self, address, value) :
        packet = DCC.BA_Packet(address, value)
        self.send(packet)
        self.send(DCC.Packet.IDLE)
        self.send(packet)

if __name__== "__main__" :
    cs = SprogCommandStation("/dev/ttyACM1")
        
