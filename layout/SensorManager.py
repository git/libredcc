#!/usr/bin/python
# -*- coding: latin-1 -*-

from Sensor import Sensor

import io
import serial
import logging

logging.basicConfig(level=logging.DEBUG)


class SensorManager :
    """Base class to manage sensors. It encapsulate methods and
    attributes relevant for high-level layout and loco functions so
    that there is a uniform interface independent of the underlying
    hardware. Individual concrete hardware sensor manager inherit this
    class. 
    """

    def __init__(self) :
        """provide infra structure to manage sensors
        """
        self.sensors = dict()


    def provideSensor(self, address) :
        """provide a sensor with the given address. Subclass need to
        override this method to connect a sensor with the hardware.
        """

        if address not in self.sensors :
            self.sensors[address] = Sensor(address)

        return self.sensors[address]

    
class Hsi88SensorManager(SensorManager) :
    """
    Sensor manager for HSI88 S88 interface. Its hardware protocol is
    decomented at
    <http://www.ldt-infocenter.com/dokuwiki/_media/de/anleitungen/hsi88_command-codes_de.pdf> 
    """
    
    def __init__(self, device_name) :
        """connect to HSI88 interface at device_name (eg /dev/ttyACM0)
        """
        SensorManager.__init__(self)

        self.dev = serial.Serial(device_name,
                                 9600,
                                 timeout=1,
        )

        # Remove any garbage
        self.dev.write('\r')
        self.dev.read_until('\r')
        self.dev.read_all()

        # Get HSI88 version -- just FYI
        self.dev.write("v\r")
        version = self.dev.read_until('\r')
        logging.info("HSI88 Device Version: {}".format(version))

        # Put into ASCII mode -- this is easier to parse than HEX mode.
        for i in range(2) :
            self.dev.write("t\r")
            self.dev.flush()
            mode = self.dev.read_until('\r').rstrip() #debug
            logging.debug("Mode response: <{}>".format(mode))
            if mode == "t1" :
                break
        if mode != "t1" :
            logging.warn("HSI88 not in ASCII mode")

    def setup(self, left, middle = 0, right = 0) :
        """Setup HSI88 with the number of modules given for the three
        chains left, middel, and right.
        """

        # send setup command
        setup_cmd = "s{:02x}{:02x}{:02x}\r".format(left,middle,right)
        logging.debug("Setup Command: {}".format(setup_cmd))
        self.dev.write(setup_cmd)


        # process response
        setup = self.dev.read_until('\r').rstrip()
        logging.debug("Setup response: <{}>".format(setup))

        if setup[0] != "s" :
            logging.warn("Expected setup response, but got <{}>".format(setup))
        reported = int(setup[1:], 16)
        if reported != left+middle+right :
            logging.warn("Reported modules deviates from requested modules: {}".format(reported))

        # provide space for internal state of modules
        self.state = reported * [0x0000]

    def parseSensors(self) :
        """ 
        waits on the HSI88 device for any sensor updates and calls the
        update method of the sensor.

        Side effect: sets timeout to infinite (ie blocking potentially
        forever)
        TODO: make it wait a long time and report that no data has
        been received for a long time, eg 100 seconds?
        TODO: catch any potential exceptions
        TODO: introduce threading.
        """

        self.dev.timeout = 120

        # eternal loop to read in sensor updates from the hardware
        while(True) :

            response = self.dev.read_until('\r')
            logging.debug("Sensor response: <{}>".format(response))

            
            # check sanity of response
            if response[0] != 'i' :
                logging.warn("Sensor response does not start with 'i': {}".format(response[0]))
                continue

            num_modules = int(response[1:2], 16)

            # cut processed part of response
            response = response[3:-1]
            if len(response) % 6 != 0 or len(response) // 6 != num_modules :
                logging.warn("Malformatted sensor response: {}".format(response))

            #  parse response into modules
            for i in range(0,len(response), 6) :

                v = int(response[i:i+6], 16)
                sensors = v & 0xFFFF
                module =  ((v >> 16) & 0xFF ) - 1

                
                # changes to sensor states in this module?
                changes = sensors ^ self.state[module]

                if changes != 0 :

                    self.state[module] ^= changes
                    baseAddr = module * 16

                    # update all sensors that have changes
                    mask = 1;
                    for offset in range(16) :
                        if changes & mask != 0 :
                            sensorAddr = baseAddr + offset
                            if self.state[module] & mask == 0 :
                                if sensorAddr in self.sensors:
                                    self.sensors[sensorAddr].update(Sensor.State.INACTIVE)
                                logging.debug("Sensor {} inactive.".format(sensorAddr))
                            else :
                                if sensorAddr in self.sensors:
                                    self.sensors[sensorAddr].update(Sensor.State.ACTIVE)
                                logging.debug("Sensor {} active.".format(sensorAddr))
                        mask <<= 1


if __name__== "__main__" :

    sm = Hsi88SensorManager("/dev/ttyACM2")
    sm.setup(1,2,3)
    sm.provideSensor(0)
    sm.provideSensor(15)

    while(True) :
        sm.parseSensors()

