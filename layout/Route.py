import logging

from enum import Enum

from Block import Block
from Resources import Signal

class Route :
    """Abstraction of a route.
    """

    #class State(Enum) :
    #    """TODO is this state needed?"""
    #    AVAILABLE = 0
    
    def __init__(self,
                 name,
                 down,
                 up,
                 interlocking = {} ) :
        """Create a new route.
        @param name of the route.
        @param down is the starting Block.
        @param up is the target Block.
        @param interlocking is a dict whose keys are layout resources
        and whose values describe the state that these resouces must
        have: For a route to be set, the call of lock(value) must return
        True. 
        """

        # Convenience
        self.name = name

        # Static Fields
        self.down = down
        self.up = up
        self.interlocking = interlocking
        # self.state = Route.State.AVAILABLE # TODO: do we need this
        # variable?

        # Dynamic Fields
        self.train = None

    def __str__(self) :
        # return "Route " + self.name + ": " + str(self.state) + ", "
        # + self.train
        return "Route " + self.name + ": " + self.train

    def lock(self) :
        """Locks resources in the state required for the route.
        """

        
        locked = { r for r in self.interlocking if r.lock(self, self.interlocking[r]) == True }

        # all locked?
        if len(locked) == len(self.interlocking) :
            return True

        # unwind partially locked resources:
        for k,v in locked :
            k.unlock(self)

        unavailable = set(self.interlocking) - locked;
        for (r,v) in unavailable :
            logging.info(
                """Could not acquire resource {} in state {} for route {} held by {}.
                """, r, v, self, r.owners)
            unavailable.add(r)

        return False

    def set(self, train) :
        """
        TODO also release section, signal and owner on failure.
        """

        # Route available?
        if self.train is not None :
            logging.info("{} held by {}".format(self, self.train))
            return False

        # Target block available? 
        if self.up.lock(self.train, Block.State.FREE) is False :
            logging.info("{} held by {}.".format(self.up, self.train))
            return False

        # Starting signal available?
        if self.down.upSignal.lock(self) is False :
            logging.info("{} held by {}".format(self.down.upSignal, self.down.upSignal.owner))
            self.up.unlock(self)
            return False

        # Interlocked resources available?
        if self.lock() is False:
            # logging done by lock().
            self.down.upSignal.unlock(self)
            self.up.unlock(self)
                        

        self.train = train

        # Provide for signal fall.
        self.up.downSensor.registerOnce(
            lambda state : state == Sensor.State.ACTIVE,
            self.down.upSignal.stop
            )

        # Provide for release when train in target block.
        self.up.upSensor.registerOnce(
            lambda state : state == Sensor.State.ACTIVE,
            self.release)

        logging.debug("{} set".format(self))
        
    def unlock(self) :
        """TODO Naming is unfortunate -- as all other resources use this
           for interlocking
        """
        
        for r in self.interlocking.keys() :
            r.unlock(self)

    def release(self, dummy) :

        self.unlock()
        self.down.upSignal.unlock(self)
        self.up.unlock(self)
        self.train = None

        logging.debug("{} released.".format(self))


