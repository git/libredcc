from enum import Enum

import logging

class Block :

    class State(Enum) :
        # AVAILABLE = 0
        FREE = 1
        OCCUPIED = 2
        # UNKNOWN = 3

    def __init__(self,
                 name, 
                 downSensor = None,
                 upSensor = None,
                 downSignal = None,
                 upSignal = None) :

        self.name = name
        
        self.downSensor = downSensor
        self.upSensor = upSensor
        self.downSignal = downSignal
        self.upSignal = upSignal

        self.owner = None;
        self.state = Block.State.FREE


    def lock(self, train, v) :
        if v != self.state :
            logging.info("{}: can't change to state {} requested".format(self, v))
            return False
        if v == Block.State.FREE :
            if self.state != Block.State.FREE :
                logging.info("{}: can't force to FREE".format(self))
                return False
            elif self.owner == None or self.owner == acquirer:
                self.owner = train
                return True
        else :
            return False

    def unlock(self, train) :
        if(self.owner == train) :
            self.owner == None
            return True
        else :
            logging.info("{} can't be unlocked by {}".format(self))
            return False

    def __str__(self) :

        return "Block " + self.name + ": " + str(self.state) + ", " + str(self.owner)
            

            

